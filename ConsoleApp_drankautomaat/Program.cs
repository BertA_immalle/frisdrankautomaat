﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp_drankautomaat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("drankautomaat");

            int inworp = 100;
            int teruggave = 0;
            int itemCost = 45;

            teruggave = inworp - itemCost;

            Console.WriteLine("number of 1 euro coins is " + teruggave / 100);
            teruggave %= 100;
            Console.WriteLine("number of 50 cent coins is " + teruggave / 50);
            teruggave %= 50;
            Console.WriteLine("number of 20 cent coins is " + teruggave / 20);
            teruggave %= 20;
            Console.WriteLine("number of 10 cent coins is " + teruggave / 10);
            teruggave %= 10;
            Console.WriteLine("number of 5 cent coins is " + teruggave / 5);
            teruggave %= 5;
            Console.WriteLine("number of 2 cent coins is " + teruggave / 2);
            teruggave %= 2;
            Console.WriteLine("number of 1 cent coins is " + teruggave / 1);
            teruggave %= 1;



        }
    }
}
